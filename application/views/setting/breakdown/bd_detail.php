<style type="text/css">
  .label-danger {
    background-color: #db3325;
}
</style>

<div class="span12">
         <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3><a href="<?php echo site_url(); ?>setting/Breakdown"> Breakdown </a></h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="width: 60%;font-size: 12px;">
                      Breakdown Type
                    </th>
                    </th>
                    <th style="width: 10%;font-size: 12px; text-align: center;">Code</th>
                  </tr>
                </thead>
                <tbody>

                  <?php foreach ($bd as $rs) { ?>

                  <tr class="r-eq" >
                    <td>
                      <?php echo $rs['eq_name']; ?>
                      <input type="hidden" id="eq_id" value="<?php echo $rs['eq_id']; ?>">
                    </td>
                    <td style="width: 10%;font-size: 12px; text-align: center;"><?php echo $rs['eq_code']; ?></td>
                  </tr>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

<div class="span10">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Breakdown System</h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="font-size: 12px;">
                     <div class="dropdown pull-left"> <a class="dropdown-toggle " id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="#"> System Name <i class=" icon-caret-down"></i> </a>
                          <ul class="dropdown-menu " role="menu" aria-labelledby="dLabel">
                            <li style="cursor: pointer;"><a class="ad-system"><i class=" icon-plus icon-small"></i>&nbsp; &nbsp; Add</a></li>
                          </ul>
                        </div>
                    </th>
                    <th style="font-size: 12px; text-align: center;">Code</th>
                    <th style="width: 15%;"> </th>
                  </tr>
                </thead>
                <tbody>

                <?php if($eq_detail){ ?>


                  <?php foreach ($eq_detail as $rs) { ?>

                    
                    <tr class="r-bd" data-bd_id="<?php echo $rs['bd_id']; ?>" data-bd_name="<?php echo $rs['bd_name']; ?>" data-bd_code="<?php echo $rs['bd_code']; ?>">
                      <td style="font-size: 12px; "><?php echo $rs['bd_name']; ?></td>
                      <td style="font-size: 12px; text-align: center;"><?php echo $rs['bd_code']; ?></td>
                      <td class="td-actions">
                        <a href="<?php echo site_url(); ?>Setting/BreakdownSystemDetail/<?php echo $rs['bd_id']; ?>" class="btn btn-small btn-success" title="Add"><i class="icon-plus icon-small"> </i></a>
                        <a class="btn btn-small btn-warning edit-bd-system" title="Edit"><i class=" icon-pencil icon-small"> </i></a>
                        <a class="btn-small btn btn-danger del-bd-system" title="Delete"><i class="icon-trash icon-small"> </i></a>
                      </td>
                    </tr>
                    <?php } ?>
                    <?php } else { ?>
                    <tr>
                      <td colspan="4" style="text-align: center;">-No Data-</td>
                    </tr>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

  <!-- Add EQ -->
  <div class="modal fade" id="add-eq-type" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Breakdown System</h4>
        </div>
        <div class="modal-body">
              <label for="eq_code">System Name:</label>
              <input type="text" class="form-control" id="bd_name" name="bd_name" style="width: 500px;">
              <input type="hidden" id="add_eq_type">

              <label for="eq_code">System Code:</label>
              <input type="text" class="form-control" id="bd_code" name="bd_code" style="width: 500px;">
              <p id="msg-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success save-system" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

    <!-- Add EQ -->
  <div class="modal fade" id="edit-bd-form" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Breakdown System</h4>
        </div>
        <div class="modal-body">
              <label for="eq_code">System Name:</label>
              <input type="text" class="form-control" id="edit_bd_name" name="edit_bd_name" style="width: 500px;">
              <input type="hidden" id="edit_bd_id">

              <label for="eq_code">System Code:</label>
              <input type="text" class="form-control" id="edit_bd_code" name="edit_bd_code" style="width: 500px;">
              <p id="msg-error-edit-bd" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success edit-system" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

      <!-- Delete EQ -->
  <div class="modal fade" id="del-bd-form" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Breakdown System</h4>
        </div>
        <div class="modal-body">
              <p>Confirm to delete ?</p>
              <input type="hidden" id="del_bd_id" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success cf-del-bd" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>


<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

  $("table").off("click", ".ad-system");
  $("table").on("click", ".ad-system", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-eq');
      var eq_id = $('#eq_id').val();

      $('#add_eq_type').val(eq_id);

       $('#add-eq-type').modal('show');


  });

  $('.save-system').click(function(){
       
      var bd_name = $('#bd_name').val();
      var bd_code = $('#bd_code').val();
      var eq_type = $('#add_eq_type').val();

      if(bd_name == ''){
        $('#msg-error-eq').html('*Please Input System Name');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/SaveBreakdownSystem',
          data:{ bd_name:bd_name, eq_type:eq_type, bd_code:bd_code}
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-error-eq').html(o.msg);
            }
            

            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }

  });

  $("table").off("click", ".edit-bd-system");
  $("table").on("click", ".edit-bd-system", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-bd');
      var bd_id = $row.data('bd_id');
      var bd_name = $row.data('bd_name');
      var bd_code = $row.data('bd_code');

      $('#edit_bd_name').val(bd_name);
      $('#edit_bd_id').val(bd_id);
      $('#edit_bd_code').val(bd_code);

       $('#edit-bd-form').modal('show');


  });

  $('.edit-system').click(function(){
       
      var bd_name = $('#edit_bd_name').val();
      var bd_code = $('#edit_bd_code').val();
      var bd_id = $('#edit_bd_id').val();

      if(bd_name == ''){
        $('#msg-error-edit-bd').html('*Please Input System Name');
      } else if(bd_code == ''){
        $('#msg-error-edit-bd').html('*Please Input System Code');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/EditBreakdownSystem',
          data:{ bd_name:bd_name, bd_id:bd_id, bd_code:bd_code}
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-error-edit-bd').html(o.msg);
            }
            

            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }

  });

  $("table").off("click", ".del-bd-system");
  $("table").on("click", ".del-bd-system", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-bd');
      var bd_id = $row.data('bd_id');

      $('#del_bd_id').val(bd_id);

      $('#del-bd-form').modal('show');


  });


  $('.cf-del-bd').click(function(){
       
      var bd_id = $('#del_bd_id').val();

        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/DelBreakdownSystem',
          data:{bd_id:bd_id}
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'complete'){
              location.reload();
            }
        })


  });

 
});         
</script>
