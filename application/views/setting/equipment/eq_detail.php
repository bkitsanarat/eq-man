<style type="text/css">
  .label-danger {
    background-color: #db3325;
}
</style>

<div class="span10">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3><a href="<?php echo site_url(); ?>setting/Equipment">Equipment Type</a></h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="width: 60%;font-size: 12px;">
                     Equipment
                    </th>
                    <th style="width: 10%;font-size: 12px; text-align: center;">Code</th>
                    <th style="width: 10%;font-size: 12px; text-align: center;">Count</th>
                    <th style="width: 10%;font-size: 12px; text-align: center;"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php foreach ($eq as $rs) { ?>

                  <tr class="r-eq" data-eq_id="<?php echo $rs['eq_id']; ?>" data-eq_name="<?php echo $rs['eq_name']; ?>" data-eq_code="<?php echo $rs['eq_code']; ?>">
                    <td>
                      <?php if(!empty($rs['eq_name'])){ echo $rs['eq_name']; } else { echo "-"; } ?>
                    </td>
                    <td style="width: 10%;font-size: 12px; text-align: center;"><?php echo $rs['eq_code']; ?></td>
                    <td style="width: 10%;font-size: 12px; text-align: center;"><?php echo $rs['eq_count']; ?></td>
                    <td style="width: 10%;font-size: 12px; text-align: center;"><a class="btn btn-small btn-success set-eq" title="Add"><i class="icon-plus icon-small"> </i></a></td>
                  </tr>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

<div class="span6">
        <!-- /widget -->
          <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Equipment Detail</h3>
              <div align="right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered" style="font-size: 12px;">
                <thead>
                  <tr>
                    <th style="font-size: 12px; text-align: center;">
                     Number
                    </th>
                    <th style="font-size: 12px; text-align: center;">Status</th>
                    <th style=""> </th>
                  </tr>
                </thead>
                <tbody>

                  <?php foreach ($eq_detail as $rs) { ?>

                    <?php if($eq_detail){ ?>

                    <tr class="r-eq" data-eqd_id="<?php echo $rs['eqd_id']; ?>" data-eq_type="<?php echo $rs['eq_type']; ?>">
                      <td style="font-size: 12px; text-align: center;"><?php echo $rs['eq_name'].' '.$rs['eq_no']; ?></td>
                      <td style="font-size: 12px; text-align: center;">
                        <?php if($rs['eq_status'] == 'Available' ){ ?>
                          <span class="label label-success">Available</span>
                        <?php } else if($rs['eq_status'] == 'Maintenance' ) { ?>
                          <span class="label label-warning">Maintenance</span>
                        <?php } else if($rs['eq_status'] == 'Breakdown' ) { ?>
                          <span class="label label-danger">Breakdown</span>
                        <?php } ?>
                      </td>
                      <td class="td-actions">
                        <a class="btn-small btn btn-danger del-eq-type" title="Delete"><i class="icon-trash icon-small"> </i></a>
                      </td>
                    </tr>

                    <?php } else { ?>


                    <?php } ?>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>
</div>

  <!-- Add EQ -->
  <div class="modal fade" id="add-eq-type" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Equipment</h4>
        </div>
        <div class="modal-body">
              <label for="eq_code">Equipment Number:</label>
              <input type="number" class="form-control" id="eq_no" name="eq_no" style="width: 500px;">
              <input type="hidden" id="add_eq_type">
              <p id="msg-error-eq" style="color: red;"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success save-eq" >Save</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
      
    </div>
  </div>

      <!-- Delete EQ -->
  <div class="modal fade" id="del-eq-type" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Equipment</h4>
        </div>
        <div class="modal-body">
              <p>Confirm to delete ?</p>
              <input type="hidden" id="del_eqd_id" value="">
              <input type="hidden" id="del_eqd_type" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success cf-del-eq" >Yes</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>


<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){

  $("table").off("click", ".set-eq");
  $("table").on("click", ".set-eq", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-eq');
      var eq_id = $row.data('eq_id');

      $('#add_eq_type').val(eq_id);

       $('#add-eq-type').modal('show');


  });

  $('.save-eq').click(function(){
       
      var eq_no = $('#eq_no').val();
      var eq_type = $('#add_eq_type').val();

      if(eq_no == ''){
        $('#msg-error-eq').html('*Please Input Equipment Number');
      } else {
        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/SaveEquipmentNumber',
          data:{ eq_no:eq_no, eq_type:eq_type}
        }).done(function(data){
            var o = JSON.parse(data);

            if(o.code_m == 'error'){
              $('#msg-error-eq').html(o.msg);
            }
            

            if(o.code_m == 'complete'){
              location.reload();
            }
        })

      }

  });

  $("table").off("click", ".del-eq-type");
  $("table").on("click", ".del-eq-type", function(e) {
      e.preventDefault();

      var $row = $(this).parents('tr.r-eq');
      var eqd_id = $row.data('eqd_id');
      var eq_type = $row.data('eq_type');

      $('#del_eqd_id').val(eqd_id);
      $('#del_eqd_type').val(eq_type);

      $('#del-eq-type').modal('show');

  });

  $('.cf-del-eq').click(function(){
       
      var del_eqd_id = $('#del_eqd_id').val();
      var del_eqd_type = $('#del_eqd_type').val();

        $.ajax({
          type:'POST',
          url:'<?php echo site_url(); ?>Setting/DelEquipmentDetail',
          data:{ del_eqd_id:del_eqd_id, del_eqd_type:del_eqd_type}
        }).done(function(data){
            var o = JSON.parse(data);
            alert(o.msg);

            if(o.code_m == 'complete'){
              location.reload();
            }
        })
  });       

});         
</script>
