<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>EQMS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>public/css/bootstrap-responsive.min.css" rel="stylesheet">
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet">
<link href="<?php echo base_url(); ?>public/css/font-awesome.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>public/css/style.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>public/css/pages/dashboard.css" rel="stylesheet">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-fixed-top" id="header-bar">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">

      <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="<?php echo site_url(); ?>">  
       EQMS</a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i>  <?php echo $user->name; ?><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo site_url(); ?>Logout">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
    <?php echo (isset($menu) ? $menu : "")?>  
<!-- /subnavbar -->

  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php echo (isset($header) ? $header : "")?>    
        <?php echo (isset($main) ? $main : "")?>   

      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 



<!-- Le javascript
================================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script> 
<script src="<?php echo base_url(); ?>public/js/excanvas.min.js"></script> 
<script src="<?php echo base_url(); ?>public/js/chart.min.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>public/js/bootstrap.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo base_url(); ?>public/js/full-calendar/fullcalendar.min.js"></script>
 
<script src="<?php echo base_url(); ?>public/js/base.js"></script> 

</body>
</html>
