<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require('Welcome.php');

class Admin extends Welcome {

	 public function __construct() {
        parent:: __construct();
        $this->load->helper("url");
        $this->load->library("pagination");
    }

	public function Index(){
		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');	
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

			if($username->role == "ADMIN"){ 	

				//show vat list
				$query_vat = $this->db->get_where('user', array('is_delete' => 0));
				$data['user'] = $query_vat->result_array();

				$this->view['main'] =  $this->load->view('admin/user',$data,true);
				$this->view();
			} else {
				redirect('Dashboard');
			}

		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');
		}

	}

	public function SaveUsers(){

		$id = $this->input->post('id');
	 	$username = $this->input->post('username');
		$name = $this->input->post('name');
		$permission = $this->input->post('permission');


		if(empty($id)){
			$check_username = $this->db->get_where('user', array('username' => $username , 'is_delete' => 0));
			$check_name = $this->db->get_where('user', array('name' => $name , 'is_delete' => 0));

			if($check_username->num_rows() > 0){
				$result['msg'] = "This username is already in use.";
				echo json_encode($result);
				return false;
			} 

			if($check_name->num_rows() > 0){
				$result['msg'] = "This name is already in use.";
				echo json_encode($result);
				return false;
			} 

		
			$data = array(
          			"username" => $username,
          			"name" => $name,
          			"password" => md5('LcitB5C3'),
          			"role" => $permission,
          			"is_change_pass" => 0,
          			"created" => date('Y-m-d H:i:s'),
          			"updated" => date('Y-m-d H:i:s')
          		);

			$this->db->insert('user', $data);

				$result['msg'] = "success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
		} else {

			$check_username = $this->db->get_where('user', array('username' => $username , 'id !=' => $id  , 'is_delete' => 0));
			$check_name = $this->db->get_where('user', array('name' => $name , 'id !=' => $id   , 'is_delete' => 0));

			if($check_username->num_rows() > 0){
				$result['msg'] = "This username is already in use.";
				echo json_encode($result);
				return false;
			} 

			if($check_name->num_rows() > 0){
				$result['msg'] = "This name is already in use.";
				echo json_encode($result);
				return false;
			} 


			$data = array(
          			"username" => $username,
          			"name" => $name,
          			"role" => $permission,
          			"updated" => date('Y-m-d H:i:s')
          		);

			$this->db->where('id', $id);
			$this->db->update('user', $data);

				$result['msg'] = "success";
				$result['code_m'] = "complete";
				echo json_encode($result);
				return false;
		}
			


	}

	public function Reset(){
		$id = $this->input->post('id');

		$data = array(

          	"password" => md5('LcitB5C3'),
          	"is_change_pass" => 0,
          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('id', $id);
		$this->db->update('user',$data);

		$result['msg'] = "success";
		$result['code_m'] = "complete";
		echo json_encode($result);
		return false;
	}

	public function DeleteUsers(){
		$id = $this->input->post('id');

		$data = array(

	          	"is_delete" => 1,
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('id', $id);
		$this->db->update('user',$data);

		$result['msg'] = "success";
		$result['code_m'] = "complete";
		echo json_encode($result);
		return false;
	}

	public function Permission(){
		
		if($this->session->userdata('logged_in')) { 	

			$check_data = $this->session->userdata('logged_in');		
			$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

				if($username->role == "ADMIN"){ 

					$check_data = $this->session->userdata('logged_in');
					$username = $this->db->get_where('user', array('id' => $check_data['id']))->row();

					$this->db->where('username !=',$username->username);
					$this->db->where('is_delete',0);
					$query_user = $this->db->get('user');
					$data['user']= $query_user->result_array();

					$this->view['main'] =  $this->load->view('admin/permission',$data,true);
					$this->view();

				} else {
					redirect('Start');
				}


		} else {
			$this->load->helper(array('form'));
			$this->load->view('login_view');	
		}
	}

	public function ConfigPermission(){

		$id = $this->input->post('id-per');
		$role = $this->input->post('role-per');
		
		$data = array(
	          	"role" => $role,
	          	"updated" => date('Y-m-d H:i:s')
	    );
	         
	    $this->db->where('id', $id);
		$this->db->update('user',$data);

		redirect('Admin\Permission');

	}


}