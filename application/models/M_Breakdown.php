<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Breakdown extends CI_Model {
	function __construct() {
		parent::__construct();
	}

	public function countrq($eq_id=null , $rq_id = null){

			$this->db->order_by('rq_order.created','DESC');
			$this->db->where('rq_order.is_del',0);
			$this->db->where('rq_order.rq_type','BD');
			$this->db->where('rq_order.eq_id',$eq_id);
			if(!empty($rq_id)){
				$this->db->where('rq_order.rq_id',$rq_id);
			}
			$this->db->join('bd_detail','bd_detail.bd_detail = rq_order.rq_detail');
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('bd_detail.bdd_code, bd_detail.bd_detail, eq_type.eq_name, eq_type.eq_code, rq_order.*');
			$rq_order = $this->db->get('rq_order');


			return $rq_order->num_rows();
	}

	public function fetchRq($limit, $start, $eq_id , $rq_id ){

			$this->db->limit($limit, $start);
			$this->db->order_by('rq_order.created','DESC');
			$this->db->where('rq_order.is_del',0);
			$this->db->where('rq_order.rq_type','BD');
			$this->db->where('rq_order.eq_id',$eq_id);
			if(!empty($rq_id)){
				$this->db->where('rq_order.rq_id',$rq_id);
			}
			$this->db->join('bd_detail','bd_detail.bd_detail = rq_order.rq_detail');
			$this->db->join('eq_type','eq_type.eq_id = rq_order.eq_id');
			$this->db->select('bd_detail.bdd_code, bd_detail.bd_detail, eq_type.eq_name, eq_type.eq_code, rq_order.*');
			$query  = $this->db->get('rq_order');

			if ($query->num_rows() > 0) {
	            foreach ($query->result() as $row) {
	                $data[] = $row;
	            }
	            return $data;
	        }

	        return false;

	}
}
?>

